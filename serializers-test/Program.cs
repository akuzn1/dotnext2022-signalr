using System.Diagnostics;
using System.Text.Json;
using MessagePack;
using SerializerTest;

var data = Initialize();
var elapsedTicksMP = new List<long>();
byte[]? resultMP = null;
var elapsedTicksJSON = new List<long>();
string resultJSON = string.Empty;

for(int i = 0 ; i < 1001 ; i++)
{
    Stopwatch swMP = new Stopwatch();
    swMP.Start();
    resultMP = MessagePackSerializer.Serialize<TestLevel>(data);
    swMP.Stop();
    elapsedTicksMP.Add(swMP.ElapsedTicks);

    Stopwatch swJSON = new Stopwatch();
    swJSON.Start();
    resultJSON =  JsonSerializer.Serialize<TestLevel>(data);
    swJSON.Stop();
    elapsedTicksJSON.Add(swJSON.ElapsedTicks);
}

Console.WriteLine("Message Pack");
Console.WriteLine($"\tDuration: {elapsedTicksMP.OrderBy(p => p).ToList()[elapsedTicksMP.Count / 2]}");
Console.WriteLine($"\tLength: {resultMP?.Length}");
Console.WriteLine("JSON");
Console.WriteLine($"\tDuration: {elapsedTicksJSON.OrderBy(p => p).ToList()[elapsedTicksJSON.Count / 2]}");
Console.WriteLine($"\tLength: {resultJSON?.Length}");

TestLevel Initialize()
{
    Random rnd = new Random();
    var level = new TestLevel() 
    {
        Id = Guid.NewGuid(),
        Title = "Test Level",
        TestObjects = new List<TestObject>(),
    };

    for(int i = 0 ; i < 50 ; i++)
    {
        level.TestObjects.Add(new TestObject()
            {
                Id = Guid.NewGuid(),
                Title = "Test Object" + Guid.NewGuid(),
                IsActive = true,
                X = rnd.NextDouble() * 1000,
                Y = rnd.NextDouble() * 1000,
                Angle = rnd.NextDouble()
            });
    }

    return level;
}

