using System.Runtime.Serialization;
using MessagePack;

namespace SerializerTest;

[MessagePackObject]
public class TestLevel
{
    [Key(0)]
    public Guid Id { get; set; }

    [Key(1)]
    public string? Title { get; set; }

    [Key(2)]
    public List<TestObject>? TestObjects { get; set; }
}