using System.Runtime.Serialization;
using MessagePack;

namespace SerializerTest;

[MessagePackObject]
public class TestObject
{
    [Key(0)]
    public Guid Id { get; set; }

    [Key(1)]
    public string? Title { get; set; }

    [Key(2)]
    public bool IsActive { get; set; }

    [Key(3)]
    public double X { get; set; }

    [Key(4)]
    public double Y { get; set; }

    [Key(5)]
    public double Angle { get; set; }
}