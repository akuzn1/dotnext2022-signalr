# DotNext2022 SignalR

Это репозиторий с исходными кодами к докладу "Введение в SignalR", прочитанному на конференции DotNext, ноябрь 2022.

Проекты создавались и тестировались со следующей конфигурацией ПО:
- Visual Studio Code 1.73.0
- .NET 7.0.100 SDK
- SignalR for ASPNetCore
- JQuery 3.5.1
- Sqlite 3.35.5

Проекты:
- basic-сhat - демонстрационный проект, основанный на пошаговой инструкции https://learn.microsoft.com/ru-ru/aspnet/core/tutorials/signalr?view=aspnetcore-7.0&tabs=visual-studio-code
- serializers-test - тестовый проект для измерения скорости работы сериалайзеров
- space-carrier-demo - демонстрационный проект-игрушка. Для корректного запуска необходимо использовать стартовую конфигурацию All (в VS Code), или другим способом одновременно стартовать три проекта: SpaceCarrier.API, SpaceCarrier.UI, SpaceCarrier.AI
