using SpaceCarrier.AI;
using SpaceCarrier.Library.Services;
using SpaceCarrier.Library.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using SpaceCarrier.Library.Repository;
using SpaceCarrier.Library.Repository.Interfaces;

IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices(services =>
    {
        services.AddTransient<IUserService, UserService>();
        services.AddTransient<ILevelService, LevelService>();
        services.AddTransient<IUpdaterService, UpdaterService>();

        var path = System.IO.Path.Join(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "game.sqlite");

        var options = new DbContextOptionsBuilder<GameDBContext>().UseSqlite($"Data Source={path}").Options;
        services.AddTransient<GameDBContext>(p => { return new GameDBContext(options); } );
        services.AddTransient<IContextFactory>(p => { return new ContextFactory(path); } );

        services.AddHostedService<Worker>();
    })
    .Build();

await host.RunAsync();
