using System.Text.Json.Serialization;
using System.Threading.Channels;
using Microsoft.AspNetCore.SignalR.Client;
using SpaceCarrier.Library.Models;
using SpaceCarrier.Library.Services.Interfaces;

namespace SpaceCarrier.AI;

public class Worker : BackgroundService
{
    private readonly string serverPath = "https://localhost:7001";
    private readonly ILogger<Worker> _logger;
    private readonly IUpdaterService _updaterService;
    private HubConnection _connection;

    public Worker(IUpdaterService levelService, ILogger<Worker> logger)
    {
        _logger = logger;
        _updaterService = levelService;

        _connection = new HubConnectionBuilder()
                .WithUrl($"{serverPath}/gameHub")
                .WithAutomaticReconnect()
                .AddMessagePackProtocol()
                .Build();
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        try
        {
            await _connection.StartAsync();
        }
        catch (Exception ex)
        {
            _logger.LogError(ex.Message);
            throw;
        }

        var timer = new PeriodicTimer(TimeSpan.FromMilliseconds(30));
        
        var channel = Channel.CreateUnbounded<GameObject>();
        await _connection.SendAsync("MovedStream", channel.Reader);

        while (await timer.WaitForNextTickAsync(stoppingToken) && !stoppingToken.IsCancellationRequested)
        {
            Guid guid = Guid.NewGuid();
            _logger.LogInformation($"{DateTime.UtcNow} start executing AI iteration {guid}");
            try
            {
                var newAsteroids = await _updaterService.CreateAsteroidsAsync();
                foreach(var asteroid in newAsteroids)
                {
                    await _connection.InvokeAsync("AsteroidAddedAsync", asteroid);
                }

                var processedObjects = await _updaterService.UpdatePositionsAsync();
                foreach(var processedObject in processedObjects)
                {
                    if(processedObject.ActionType == null)
                        await channel.Writer.WriteAsync(processedObject);
                    else
                    {
                        switch((ActionType)processedObject.ActionType)
                        {
                            case ActionType.Jump: 
                                await _connection.InvokeAsync("ShipJump", processedObject);
                                break;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message);
            }
            _logger.LogInformation($"{DateTime.UtcNow} stop executing AI iteration {guid}");
        }

        channel.Writer.Complete();
    }
}
