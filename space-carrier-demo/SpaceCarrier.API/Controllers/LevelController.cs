using Microsoft.AspNetCore.Mvc;
using SpaceCarrier.Library.Models;
using SpaceCarrier.Library.Services.Interfaces;

namespace SpaceCarrier.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class LevelController : ControllerBase
{
    private readonly ILogger<LevelController> _logger;
    private readonly ILevelService _levelService;

    public LevelController(ILevelService levelService, ILogger<LevelController> logger)
    {
        _logger = logger;
        _levelService = levelService;
    }

    [HttpGet]
    public async Task<Level> GetAsync([FromQuery]Guid userId)
    {
        return await _levelService.GetLevelAsync(userId);
    }
}
