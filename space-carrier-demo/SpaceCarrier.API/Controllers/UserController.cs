using Microsoft.AspNetCore.Mvc;
using SpaceCarrier.Library.Models;
using SpaceCarrier.Library.Services.Interfaces;

namespace SpaceCarrier.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class UserController : ControllerBase
{
    private readonly ILogger<LevelController> _logger;
    private readonly IUserService _userService;

    public UserController(IUserService userService, ILogger<LevelController> logger)
    {
        _logger = logger;
        _userService = userService;
    }

    [HttpGet]
    public async Task<User> GetAsync([FromQuery]Guid userId)
    {
        return await _userService.GetUserAsync(userId);
    }

    [HttpPost]
    public async Task<User> CreateAsync([FromBody]User user)
    {
        return await _userService.CreateUserAsync(user);
    }
}