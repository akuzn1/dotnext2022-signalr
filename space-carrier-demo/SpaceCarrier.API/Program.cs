using System.Text.Json.Serialization;
using Microsoft.EntityFrameworkCore;
using SpaceCarrier.Api.Extensions;
using SpaceCarrier.Api.Hubs;
using SpaceCarrier.Library.Repository;
using SpaceCarrier.Library.Services;
using SpaceCarrier.Library.Services.Interfaces;

var builder = WebApplication.CreateBuilder(args);
const string _policyName = "basicPolicy";

builder.Logging.ClearProviders();
builder.Logging.AddConsole();

// Add services to the container.
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: _policyName,
        policy  =>
            {
                policy
                    .WithOrigins("https://localhost:7044", "http://localhost:7044")
                    .AllowAnyHeader()
                    .AllowCredentials()
                    .AllowAnyMethod();
            });
});

builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<ILevelService, LevelService>();

var path = System.IO.Path.Join(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "game.sqlite");
builder.Services.AddDbContext<GameDBContext>(
        options => options.UseSqlite($"Data Source={path}"));

builder.Services.AddControllers()
    .AddJsonOptions(x => x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddSignalR()
    .AddMessagePackProtocol();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseMiddleware<ExceptionMiddleware>();

app.UseHttpsRedirection();
app.UseRouting();

app.UseCors(_policyName);

app.UseAuthorization();

app.MapControllers().RequireCors(_policyName);
app.MapHub<GameHub>("/gameHub");

app.Run();