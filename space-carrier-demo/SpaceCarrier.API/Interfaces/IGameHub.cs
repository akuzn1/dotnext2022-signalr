using SpaceCarrier.Library.Models;

namespace SpaceCarrier.Api.Interfaces;

public interface IGameClient
{
    Task NewUser(User user);
    Task NewObject(GameObject obj);
    Task GameObjectMoved(GameObject obj);
    Task RemoveUser(User user);
    Task RemoveObject(GameObject obj);
    Task PositionChanged(User user);
    Task ShipJump(User user);
}