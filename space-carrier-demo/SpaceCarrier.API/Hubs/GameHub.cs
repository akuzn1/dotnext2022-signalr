using Microsoft.AspNetCore.SignalR;
using SpaceCarrier.Api.Interfaces;
using SpaceCarrier.Library;
using SpaceCarrier.Library.Models;
using SpaceCarrier.Library.Services.Interfaces;

namespace SpaceCarrier.Api.Hubs
{
    public class GameHub : Hub<IGameClient>
    {
        private readonly ILogger<GameHub> _logger;
        private readonly IUserService _userService;

        public GameHub(IUserService userService, ILogger<GameHub> logger)
        {
            _logger = logger;
            _userService = userService;
        }

        public async Task UserConnectedAsync(Guid userId)
        {
            var user = await _userService.GetUserAsync(userId);

            if(user == null)
                throw new AppException($"Invalid user state id = {userId}");
            
            await _userService.ActivateUserAsync(userId, Context.ConnectionId);

            await Groups.AddToGroupAsync(Context.ConnectionId, user.Ship.LevelId.ToString());
            await Clients.Group(user.Ship.LevelId.ToString()).NewUser(user);
        }

        public async Task AsteroidAddedAsync(GameObject obj)
        {
            await Clients.Group(obj.LevelId.ToString()).NewObject(obj);
        }

        public async Task MovedStream(IAsyncEnumerable<GameObject> stream)
        {
            await foreach (var obj in stream)
            {
                await Clients.Group(obj.LevelId.ToString()).GameObjectMoved(obj);
            }
        }

        public async Task UserClickAsync(Guid userId, double x, double y)
        {
            var user = await _userService.HandleClickAsync(userId, x, y);
            await Clients.Group(user.Ship.LevelId.ToString()).PositionChanged(user);
        }

        public async Task<int> GameObjectClickAsync(Guid userId, Guid objectId)
        {
            var gameObject = await _userService.HandleObjectClickAsync(userId, objectId);

            var user = await _userService.GetUserAsync(userId);

            if(gameObject != null)
                await Clients.Group(user.Ship.LevelId.ToString()).RemoveObject(gameObject);

            return user.Score;
        }

        public async Task ShipJump(GameObject ship)
        {
            if(ship.ActionParameters == null) 
                throw new AppException("invalid parameters");

            var user = await _userService.GetUserByShipIdAsync(ship.Id);
            if(user.ConnectionString != null)
            {
                await Groups.RemoveFromGroupAsync(user.ConnectionString, ship.LevelId.ToString());
                await Clients.Group(ship.ActionParameters).RemoveObject(ship);
                await Clients.Group(ship.LevelId.ToString()).NewObject(ship);
                await Clients.Client(user.ConnectionString).ShipJump(user);
                await Groups.AddToGroupAsync(user.ConnectionString, ship.LevelId.ToString());
                await _userService.ClearActions(user.Id);
            }
        }

        public override async Task OnDisconnectedAsync (Exception? exception)
        {
            var user = await _userService.DeactivateUserAsync(Context.ConnectionId);
            if(user != null && user.Ship != null)
            {
                await Groups.RemoveFromGroupAsync(Context.ConnectionId, user.Ship.LevelId.ToString());
                await Clients.Group(user.Ship.LevelId.ToString()).RemoveUser(user);
            }
        }
    }
}