namespace SpaceCarrier.Library.Models;

public class MoveAction 
{
    public Guid LevelId { get; set; }
    public double X { get; set; }
    public double Y { get; set; }
}