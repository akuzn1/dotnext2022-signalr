using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace SpaceCarrier.Library.Models;

[DataContract]
public class User
{
    [Key]
    [DataMember(Name = "id")]
    public Guid Id { get; set; }

    [DataMember(Name = "name")]
    public string? Name { get; set; }

    [DataMember(Name = "connectionString")]
    public string? ConnectionString { get; set; }

    [DataMember(Name = "shipId")]
    public Guid ShipId { get; set; }

    [ForeignKey(nameof(ShipId))]
    [DataMember(Name = "ship")]
    public GameObject Ship { get; set; } = new();

    [DataMember(Name = "score")]
    public int Score { get; set; }
}