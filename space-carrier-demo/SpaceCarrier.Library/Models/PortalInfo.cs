namespace SpaceCarrier.Library.Models;

public class PortalInfo
{
    public Guid LevelId { get; set; }
    public int X { get; set; }
    public int Y { get; set; }
}