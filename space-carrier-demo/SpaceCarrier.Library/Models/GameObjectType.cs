namespace SpaceCarrier.Library.Models;

public enum GameObjectType
{
    Station = 1,
    Portal = 2,
    Ship = 3,
    Asteroid = 4
}