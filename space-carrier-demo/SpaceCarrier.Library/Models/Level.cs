using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace SpaceCarrier.Library.Models;

[DataContract]
public class Level
{
    [Key]
    [DataMember(Name = "id")]
    public Guid Id { get; set; }

    [DataMember(Name = "isInitial")]
    public bool IsInitial { get; set; }

    [DataMember(Name = "title")]
    public string? Title { get; set; }

    [DataMember(Name = "backgroungLink")]
    public string? BackgroundLink { get; set; }

    [DataMember(Name = "width")]
    public int Width { get; set; }

    [DataMember(Name = "height")]
    public int Height { get; set; }    

    [DataMember(Name = "gameObjects")]
    public ICollection<GameObject> GameObjects { get; set; } = new HashSet<GameObject>(); 
}