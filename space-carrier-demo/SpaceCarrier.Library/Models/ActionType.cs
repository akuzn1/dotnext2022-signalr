namespace SpaceCarrier.Library.Models;

public enum ActionType
{
    Jump = 1,
    Trade = 2
}