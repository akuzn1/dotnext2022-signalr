using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace SpaceCarrier.Library.Models;

[DataContract]
public class GameObject
{
    [Key]
    [DataMember(Name = "id")]
    public Guid Id { get; set; }
    
    [DataMember(Name = "levelId")]
    public Guid LevelId { get; set; }

    [ForeignKey(nameof(LevelId))]
    public Level? Level { get; set; }

    [DataMember(Name = "title")]
    public string? Title { get; set; }

    [DataMember(Name = "imageLink")]
    public string? ImageLink { get; set; }

    [DataMember(Name = "isActive")]
    public bool IsActive { get; set; }

    [DataMember(Name = "x")]
    public double X { get; set; }

    [DataMember(Name = "y")]
    public double Y { get; set; }

    [DataMember(Name = "width")]
    public int Width { get; set; }

    [DataMember(Name = "height")]
    public int Height { get; set; }

    [DataMember(Name = "gameObjectType")]
    public GameObjectType GameObjectType { get; set; }

    [DataMember(Name = "dx")]
    public double DX { get; set; }

    [DataMember(Name = "dy")]
    public double DY { get; set; }

    [DataMember(Name = "targetX")]
    public double TargetX { get; set; }

    [DataMember(Name = "targetY")]
    public double TargetY { get; set; }

    [DataMember(Name = "angle")]
    public double Angle { get; set; }

    [DataMember(Name = "dAngle")]
    public double DAngle { get; set; }

    [DataMember(Name = "actionType")]
    public int? ActionType { get; set; }

    [DataMember(Name = "actionParameters")]
    public string? ActionParameters { get; set; }
}