﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SpaceCarrier.Library.Migrations
{
    public partial class AddLevelsData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Levels",
                columns: new[] { "Id", "IsInitial", "Title", "BackgroundLink", "Width", "Height" },
                values: new object[] { new Guid("e3b105b7-2f92-4643-8a49-90b5243ef611"), true, "Низкие орбиты", "/images/levels/level1.jpg", 1920, 1380 }
            );

            migrationBuilder.InsertData(
                table: "Levels",
                columns: new[] { "Id", "IsInitial", "Title", "BackgroundLink", "Width", "Height" },
                values: new object[] { new Guid("61d1c7c4-238a-4129-a5e1-58a8dca2d648"), false, "Бездна космоса", "/images/levels/level2.jpg", 1920, 1311 }
            );

            migrationBuilder.InsertData(
                table: "Levels",
                columns: new[] { "Id", "IsInitial", "Title", "BackgroundLink", "Width", "Height" },
                values: new object[] { new Guid("c3157733-08a6-4c03-877b-289c19b0d004"), false, "Осколки сверхновой", "/images/levels/level3.jpg", 1920, 1385 }
            );

            migrationBuilder.InsertData(
                table: "GameObjects",
                columns: new[] { "Id", "LevelId", "Title", "ImageLink", "IsActive", "GameObjectType", "X", "Y", "TargetX", "TargetY", "Width", "Height", "DX", "DY", "Angle", "DAngle", "ActionType", "ActionParameters"},
                values: new object[] { new Guid("972ec1fd-67ef-4eca-97e1-8c2814486267"), new Guid("e3b105b7-2f92-4643-8a49-90b5243ef611"), "Базовая станция", "/images/stations/basestation.png", true, 1, 140, 540, 140, 540, 240, 240, 0, 0, 0, 0.1, 2, null}
            );

            migrationBuilder.InsertData(
                table: "GameObjects",
                columns: new[] { "Id", "LevelId", "Title", "ImageLink", "IsActive", "GameObjectType", "X", "Y", "TargetX", "TargetY", "Width", "Height", "DX", "DY", "Angle", "DAngle", "ActionType", "ActionParameters"},
                values: new object[] { new Guid("01f57b75-158f-4a18-a0bd-28502e499a74"), new Guid("e3b105b7-2f92-4643-8a49-90b5243ef611"), "Портал в \"Бездна космоса\"", "/images/stations/portal.png", true, 2, 140, 140, 140, 140, 250, 250, 0, 0, 0, 0.1, 1, "{\"LevelId\":\"61d1c7c4-238a-4129-a5e1-58a8dca2d648\", \"X\":140, \"Y\":140}"}
            );

            migrationBuilder.InsertData(
                table: "GameObjects",
                columns: new[] { "Id", "LevelId", "Title", "ImageLink", "IsActive", "GameObjectType", "X", "Y", "TargetX", "TargetY", "Width", "Height", "DX", "DY", "Angle", "DAngle", "ActionType", "ActionParameters"},
                values: new object[] { new Guid("cc8fcb08-42da-472a-a831-e50c4f82a8c2"), new Guid("61d1c7c4-238a-4129-a5e1-58a8dca2d648"), "Портал в \"Низкие орбиты\"", "/images/stations/portal.png", true, 2, 140, 140, 140, 140, 250, 250, 0, 0, 0, 0.1, 1, "{\"LevelId\":\"e3b105b7-2f92-4643-8a49-90b5243ef611\", \"X\":140, \"Y\":140}"}
            );

            migrationBuilder.InsertData(
                table: "GameObjects",
                columns: new[] { "Id", "LevelId", "Title", "ImageLink", "IsActive", "GameObjectType", "X", "Y", "TargetX", "TargetY", "Width", "Height", "DX", "DY", "Angle", "DAngle", "ActionType", "ActionParameters"},
                values: new object[] { new Guid("e91e2508-3291-44e3-bb3e-8842bef6cce1"), new Guid("61d1c7c4-238a-4129-a5e1-58a8dca2d648"), "Портал в \"Осколки сверхновой\"", "/images/stations/portal.png", true, 2, 140, 540, 140, 540, 250, 250, 0, 0, 0, 0.1, 1, "{\"LevelId\":\"c3157733-08a6-4c03-877b-289c19b0d004\", \"X\":140, \"Y\":140}"}
            );

            migrationBuilder.InsertData(
                table: "GameObjects",
                columns: new[] { "Id", "LevelId", "Title", "ImageLink", "IsActive", "GameObjectType", "X", "Y", "TargetX", "TargetY", "Width", "Height", "DX", "DY", "Angle", "DAngle", "ActionType", "ActionParameters"},
                values: new object[] { new Guid("a1d3866f-2bc0-4c7e-8388-8d217fd55d5c"), new Guid("c3157733-08a6-4c03-877b-289c19b0d004"), "Портал в \"Бездна космоса\"", "/images/stations/portal.png", true, 2, 140, 140, 140, 140, 250, 250, 0, 0, 0, 0.1, 1, "{\"LevelId\":\"61d1c7c4-238a-4129-a5e1-58a8dca2d648\", \"X\":140, \"Y\":640}"}
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData("Levels", "Id", new Guid("c3157733-08a6-4c03-877b-289c19b0d004"));
            migrationBuilder.DeleteData("Levels", "Id", new Guid("61d1c7c4-238a-4129-a5e1-58a8dca2d648"));
            migrationBuilder.DeleteData("Levels", "Id", new Guid("e3b105b7-2f92-4643-8a49-90b5243ef611"));

            migrationBuilder.DeleteData("GameObjects", "Id", new Guid("cc8fcb08-42da-472a-a831-e50c4f82a8c2"));
            migrationBuilder.DeleteData("GameObjects", "Id", new Guid("01f57b75-158f-4a18-a0bd-28502e499a74"));
            migrationBuilder.DeleteData("GameObjects", "Id", new Guid("972ec1fd-67ef-4eca-97e1-8c2814486267"));
            migrationBuilder.DeleteData("GameObjects", "Id", new Guid("e91e2508-3291-44e3-bb3e-8842bef6cce1"));
            migrationBuilder.DeleteData("GameObjects", "Id", new Guid("a1d3866f-2bc0-4c7e-8388-8d217fd55d5c"));
        }
    }
}