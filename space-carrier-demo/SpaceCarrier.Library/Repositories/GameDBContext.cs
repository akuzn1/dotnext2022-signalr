using SpaceCarrier.Library.Models;
using Microsoft.EntityFrameworkCore;

namespace SpaceCarrier.Library.Repository;
public class GameDBContext : DbContext
{
    private static bool _created = false;
    private static Mutex mut = new Mutex();

    public GameDBContext()
    {
    }

    public GameDBContext(DbContextOptions<GameDBContext> options)
    : base(options)
    {
        if (!_created)
        {
            _created = true;
            if(!Database.CanConnect())
            {
                mut.WaitOne();
                if(!Database.CanConnect())
                {
                    Database.Migrate();
                }
                mut.ReleaseMutex();
            }
        }
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        var path = System.IO.Path.Join(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "game.sqlite");
        optionsBuilder.UseSqlite($"Data Source={path}");
    }

    public DbSet<Level> Levels => Set<Level>();
    public DbSet<User> Users => Set<User>();
    public DbSet<GameObject> GameObjects => Set<GameObject>();
}
