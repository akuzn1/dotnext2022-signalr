using Microsoft.EntityFrameworkCore;
using SpaceCarrier.Library.Repository.Interfaces;

namespace SpaceCarrier.Library.Repository;

public class ContextFactory : IContextFactory
{
    private readonly string _path;
    public ContextFactory(string path)
    {
        _path = path;
    }

    public GameDBContext CreateContext()
    {
        var options = new DbContextOptionsBuilder<GameDBContext>().UseSqlite($"Data Source={_path}").Options;
        return new GameDBContext(options);
    }
}