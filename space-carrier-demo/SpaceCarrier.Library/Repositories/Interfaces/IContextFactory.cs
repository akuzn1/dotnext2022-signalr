namespace SpaceCarrier.Library.Repository.Interfaces;

public interface IContextFactory
{
    GameDBContext CreateContext();
}