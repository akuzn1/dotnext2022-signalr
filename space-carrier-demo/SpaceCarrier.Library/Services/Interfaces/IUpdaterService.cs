using SpaceCarrier.Library.Models;

namespace SpaceCarrier.Library.Services.Interfaces;

public interface IUpdaterService
{
    Task<List<GameObject>> CreateAsteroidsAsync();
    Task<List<GameObject>> UpdatePositionsAsync();
}