using SpaceCarrier.Library.Models;

namespace SpaceCarrier.Library.Services.Interfaces;

public interface IUserService
{
    Task<User> GetUserAsync(Guid userId);
    Task<User> GetUserByShipIdAsync(Guid shipId);
    Task<User> CreateUserAsync(User user);
    Task ActivateUserAsync(Guid userId, string connectionString);
    Task<User?> DeactivateUserAsync(string connectionString);
    Task<User> HandleClickAsync(Guid userId, double x, double y);
    Task<GameObject?> HandleObjectClickAsync(Guid userId, Guid objectId);
    Task ClearActions(Guid userId);
}