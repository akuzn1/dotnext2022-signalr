using SpaceCarrier.Library.Models;

namespace SpaceCarrier.Library.Services.Interfaces;

public interface ILevelService
{
    Task<List<Level>> GetLevelsAsync();
    Task<Level> GetLevelAsync(Guid userId);
}