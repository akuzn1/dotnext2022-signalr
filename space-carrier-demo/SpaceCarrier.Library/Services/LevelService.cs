using SpaceCarrier.Library.Models;
using SpaceCarrier.Library.Repository;
using SpaceCarrier.Library.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace SpaceCarrier.Library.Services;

public class LevelService : ILevelService
{
    private readonly GameDBContext _context;

    public LevelService(GameDBContext context)
    {
        _context = context;
    }

    public async Task<List<Level>> GetLevelsAsync()
    {
        return await _context.Levels.ToListAsync();
    }

    public async Task<Level> GetLevelAsync(Guid userId)
    {
        var user =  await _context.Users
            .Include(p => p.Ship)
            .FirstOrDefaultAsync(p => p.Id == userId);

        if(user == null || user.Ship == null)
            throw new AppException("Invalid user");

        var level = await _context.Levels
            .Include(p => p.GameObjects.Where(q => q.IsActive))
            .FirstOrDefaultAsync(p => p.Id == user.Ship.LevelId);

        if(level == null)
            throw new AppException("Invalid level id");

        level.GameObjects = level.GameObjects.OrderBy(p => p.GameObjectType).ToList();
        return level;
    }
}