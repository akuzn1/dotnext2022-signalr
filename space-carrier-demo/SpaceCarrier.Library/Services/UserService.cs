using SpaceCarrier.Library.Models;
using SpaceCarrier.Library.Repository;
using SpaceCarrier.Library.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace SpaceCarrier.Library.Services;

public class UserService : IUserService
{
    private readonly GameDBContext _context;
    private readonly Random _rnd = new Random();
    const int Distance = 200;

    public UserService(GameDBContext context)
    {
        _context = context;
    }

    public async Task<User> GetUserAsync(Guid userId)
    {
        var user =  await _context.Users
            .Include(p => p.Ship)
            .FirstOrDefaultAsync(p => p.Id == userId);

        if(user == null || user.Ship == null)
            throw new AppException($"Invalid user with id {userId}");

        return user;
    }

    public async Task<User> GetUserByShipIdAsync(Guid shipId)
    {
        var user =  await _context.Users
            .Include(p => p.Ship)
            .FirstOrDefaultAsync(p => p.ShipId == shipId);

        if(user == null || user.Ship == null)
            throw new AppException($"Invalid user with ship id {shipId}");

        return user;
    }

    public async Task ActivateUserAsync(Guid userId, string connectionString)
    {
        if(string.IsNullOrEmpty(connectionString))
            throw new AppException("Connection string can't be null or empty");

        var user = await GetUserAsync(userId);
        user.ConnectionString = connectionString;
        
        user.Ship.IsActive = true;
        
        await _context.SaveChangesAsync();
    }

    public async Task<User?> DeactivateUserAsync(string connectionString)
    {
        if(string.IsNullOrEmpty(connectionString))
            throw new AppException("Connection string can't be null or empty");

        var user = await _context.Users.Include(p => p.Ship).FirstOrDefaultAsync(p => p.ConnectionString == connectionString);
        if(user == null)
            return null;

        user.ConnectionString = null;

        user.Ship.IsActive = false;

        await _context.SaveChangesAsync();
        return user;
    }

    
    public async Task ClearActions(Guid userId)
    {
        var user = await GetUserAsync(userId);
        user.Ship.ActionType = null;
        user.Ship.ActionParameters = null;
        await _context.SaveChangesAsync();
    }

    public async Task<User> CreateUserAsync(User user)
    {
        user.Id = Guid.NewGuid();

        var startLevel = await _context.Levels.FirstOrDefaultAsync(p => p.IsInitial);
        if(startLevel == null)
            throw new AppException("There are no start level");

        var station = await _context.GameObjects.FirstOrDefaultAsync(p => p.LevelId == startLevel.Id && p.ActionType == (int)ActionType.Trade);
        if(station == null)
            throw new AppException("There are no station on the start level");

        double x = station.X + (_rnd.NextDouble() - 0.5) * Distance;
        double y = station.Y + (_rnd.NextDouble() - 0.5) * Distance;

        var ship = new GameObject() 
        {
            Id = Guid.NewGuid(),
            LevelId = startLevel.Id,
            X = x,
            Y = y,
            TargetX = x,
            TargetY = y,
            GameObjectType = GameObjectType.Ship,
            ImageLink = "/images/ships/ship1.png",
            IsActive = false,
            Title = user.Name,
            DX = 0,
            DY = 0,
            Angle = _rnd.NextDouble() * 2 * Math.PI,
            DAngle = 0,
            Width = 100,
            Height = 100
        };

        user.Ship = ship;

        await _context.GameObjects.AddAsync(ship);
        await _context.Users.AddAsync(user);
        await _context.SaveChangesAsync();

        return await GetUserAsync(user.Id);
    }

    public async Task<GameObject?> HandleObjectClickAsync(Guid userId, Guid objectId)
    {
        var user = await GetUserAsync(userId);
        if(user == null)
            throw new AppException($"Invalid user state id = {userId}");
        
        var gameObject = await _context.GameObjects.FirstOrDefaultAsync(p => p.Id == objectId);

        if(gameObject != null)
        {
            if(gameObject.GameObjectType == GameObjectType.Asteroid)
            {
                _context.GameObjects.Remove(gameObject);
                user.Score += 100;
                await _context.SaveChangesAsync();
            }
        }

        return gameObject;
    }

    public async Task<User> HandleClickAsync(Guid userId, double x, double y)
    {
        var user = await GetUserAsync(userId);

        double oldx = user.Ship.X;
        double oldy = user.Ship.Y;
        double dx = x - oldx;
        double dy = y - oldy;
        double distance = Math.Sqrt(dx*dx + dy*dy);
        double speed = 1;
        double stepPerTurn = speed / distance;

        double ndx = dx * stepPerTurn;
        double ndy = dy * stepPerTurn;

        user.Ship.DX = ndx;
        user.Ship.DY = ndy;
        user.Ship.DAngle = 0;
        user.Ship.TargetX = x;
        user.Ship.TargetY = y;
        
        double angle = 0;
        if(distance > 0.001)
            angle = Math.Asin(dy / distance);
        if(dx < 0)
            angle = Math.PI-angle;

        user.Ship.Angle = (180 / Math.PI) * angle;

        await _context.SaveChangesAsync();

        return user;
    }
}