using SpaceCarrier.Library.Models;
using SpaceCarrier.Library.Repository;
using SpaceCarrier.Library.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SpaceCarrier.Library.Repository.Interfaces;
using System.Text.Json;

namespace SpaceCarrier.Library.Services;

public class UpdaterService : IUpdaterService
{
    private readonly ILogger _logger;
    private readonly IContextFactory _contextFactory;

    public UpdaterService(IContextFactory contextFactory, ILogger<IUpdaterService> logger)
    {
        _logger = logger;
        _contextFactory = contextFactory;
    }

    private readonly Random _rnd = new Random();
    const int AsteroidsPerLevel = 50;

    public async Task<List<GameObject>> CreateAsteroidsAsync()
    {
        using(GameDBContext context = _contextFactory.CreateContext())
        {
            var levels = await context.Levels.ToListAsync();
            var asteroidWidth = 50;
            var asteroidHeight = 50;
            var asteroids = new List<GameObject>();

            foreach(var level in levels)
            {
                var asteroidsCount = await context.GameObjects.CountAsync(p => p.GameObjectType == GameObjectType.Asteroid);
                if(asteroidsCount < AsteroidsPerLevel)
                {
                    var levelAsteroids = new List<GameObject>();
                    for(int i = asteroidsCount ; i < AsteroidsPerLevel ; i++)
                    {
                        double x = _rnd.Next(asteroidWidth/2, level.Width - asteroidWidth/2);
                        double y = _rnd.Next(asteroidHeight/2, level.Height - asteroidHeight/2);

                        levelAsteroids.Add(new GameObject() {
                            Id = Guid.NewGuid(),
                            LevelId = level.Id,
                            X = x,
                            Y = y,
                            TargetX = x,
                            TargetY = y,
                            GameObjectType = GameObjectType.Asteroid,
                            ImageLink = "/images/asteroids/asteroid1.png",
                            IsActive = true,
                            DX = 0,
                            DY = 0,
                            Angle = _rnd.NextDouble() * 2 * Math.PI,
                            DAngle = _rnd.NextDouble(),
                            Width = 50,
                            Height = 50
                        });
                    }               
                    await context.GameObjects.AddRangeAsync(levelAsteroids);
                    await context.SaveChangesAsync();
                    asteroids.AddRange(levelAsteroids);
                }
            }
            return asteroids;
        }
    }

    public async Task<List<GameObject>> UpdatePositionsAsync()
    {
        using(GameDBContext context = _contextFactory.CreateContext())
        {
            var portals = await context.GameObjects.Where(p => p.GameObjectType == GameObjectType.Portal).ToListAsync();
            var processedObjects = new List<GameObject>();

            var objectsToMove = await context.GameObjects.Where(p => (p.DX != 0 || p.DY != 0) && p.ActionType == null).ToListAsync();
            foreach(var gameObject in objectsToMove)
            {
                _logger.LogDebug($"x: {gameObject.X}, y: {gameObject.Y}, dx: {gameObject.DX}, dy: {gameObject.DY}, tx: {gameObject.TargetX}, ty: {gameObject.TargetY}, id: {gameObject.Id}");

                bool isProcessed = false;
                if(gameObject.GameObjectType == GameObjectType.Ship)
                {
                    var portal = portals.FirstOrDefault(p => p.LevelId == gameObject.LevelId && 
                            gameObject.X > p.X - p.Width / 2 && gameObject.X < p.X + p.Width / 2 &&
                            gameObject.Y > p.Y - p.Height / 2 && gameObject.Y < p.Y + p.Height / 2);
                    
                    if(portal != null)
                    {
                        var jumpData = JsonSerializer.Deserialize<MoveAction>(portal.ActionParameters ?? throw new AppException($"Empty ActionParameters for portal {portal.Id}"));
                        if(jumpData == null)
                            throw new AppException($"Invalid ActionParameters for portal {portal.Id}");

                        var oldLevel = gameObject.LevelId;
                        gameObject.LevelId = jumpData.LevelId;
                        gameObject.X = jumpData.X + portal.Width;
                        gameObject.Y = jumpData.Y + portal.Height;
                        gameObject.DX = 0;
                        gameObject.DY = 0;
                        gameObject.TargetX = 0;
                        gameObject.TargetY = 0;
                        gameObject.ActionType = (int)ActionType.Jump;
                        gameObject.ActionParameters = oldLevel.ToString();
                        processedObjects.Add(gameObject);
                        isProcessed = true;
                    }
                }

                if(!isProcessed)
                {
                    if(Math.Sqrt((gameObject.TargetX - gameObject.X) * (gameObject.TargetX - gameObject.X) + (gameObject.TargetY - gameObject.Y)*(gameObject.TargetY - gameObject.Y)) 
                        < Math.Sqrt(gameObject.DX * gameObject.DX + gameObject.DY * gameObject.DY))
                    {
                        gameObject.X = gameObject.TargetX;
                        gameObject.Y = gameObject.TargetY;
                        gameObject.DX = 0;
                        gameObject.DY = 0;
                    }
                    else
                    {
                        gameObject.X += gameObject.DX;
                        gameObject.Y += gameObject.DY;
                    }
                    processedObjects.Add(gameObject);
                }
            }
            await context.SaveChangesAsync();
            return processedObjects;
        }
    }
}