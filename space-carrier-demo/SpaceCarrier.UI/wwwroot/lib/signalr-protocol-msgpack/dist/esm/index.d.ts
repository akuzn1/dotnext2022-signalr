/** The version of the SignalR Message Pack protocol library. */
export declare const VERSION = "7.0.0";
export { MessagePackHubProtocol } from "./MessagePackHubProtocol";
export { MessagePackOptions } from "./MessagePackOptions";
