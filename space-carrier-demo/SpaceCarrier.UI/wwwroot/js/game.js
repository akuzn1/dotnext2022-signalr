const serverPath = "https://localhost:7001";
var game = {};
var connection = new signalR.HubConnectionBuilder()
    .withHubProtocol(new signalR.protocols.msgpack.MessagePackHubProtocol())
    .withUrl(serverPath + "/gameHub")
    .build();

connection.on("NewUser", function (user) {
    if(user.id == localStorage.getItem("userId"))
        return;
    
    const indexOfObject = game.ships.findIndex(o => { return o.id === user.ship.id; });
    if(indexOfObject >= 0)
    {
        game.ships.splice(indexOfObject, 1);
        removeGameObject(user.ship);
    }
    const indexOfGameObject = game.gameObjects.findIndex(o => { return o.id === user.ship.id; });
    if(indexOfGameObject >= 0)
    {
        game.gameObjects.splice(indexOfGameObject, 1);
    }

    game.ships.push(user.ship);
    game.gameObjects.push(user.ship);
    addGameObject(user.ship);
});

connection.on("GameObjectMoved", function (obj) {
    const indexOfObject = game.gameObjects.findIndex(o => { return o.id === obj.id; });
    if(indexOfObject >= 0)
    {
        var gameObject = game.gameObjects[indexOfObject];
        gameObject.x = obj.x;
        gameObject.y = obj.y;
        gameObject.dx = obj.dx;
        gameObject.dy = obj.dy;
        gameObject.targetX = obj.targetX;
        gameObject.targetY = obj.targetY;
        gameObject.angle = obj.angle;
        gameObject.dAngle = obj.dAngle;
        console.log("Game object moved: (" + gameObject.x + "," + gameObject.y + "), id:" + obj.id);
    }
});

connection.on("ShipJump", function (user) {
    console.log("Ship jump. UserId:" + user.id);
    loadLevel(user.id);
});

connection.on("NewObject", function (gameObject) {
    game.gameObjects.push(gameObject);
    game.asteroids.push(gameObject);
    addGameObject(gameObject);
    document.getElementById(gameObject.id).addEventListener("mousedown", function(e) { e.preventDefault(); onGameObjectClick(gameObject.id, e.pageX, e.pageY); });
});

connection.on("RemoveObject", function (gameObject) {
    const indexOfObject = game.asteroids.findIndex(o => { return o.id === gameObject.id; });
    if(indexOfObject >= 0)
    {
        game.asteroids.splice(indexOfObject, 1);
    }
    const index = game.gameObjects.findIndex(o => { return o.id === gameObject.id; });
    if(index >= 0)
    {
        game.gameObjects.splice(index, 1);
        removeGameObject(gameObject);
    }
});

connection.on("RemoveUser", function (user) {
    const indexOfObject = game.ships.findIndex(o => { return o.id === user.ship.id; });
    if(indexOfObject >= 0)
    {
        game.ships.splice(indexOfObject, 1);
        removeGameObject(user.ship);
    }
    const indexOfGameObject = game.gameObjects.findIndex(o => { return o.id === user.ship.id; });
    if(indexOfGameObject >= 0)
    {
        game.gameObjects.splice(indexOfGameObject, 1);
    }
});

connection.on("PositionChanged", function (user) {
    const indexOfObject = game.ships.findIndex(o => { return o.id === user.ship.id; });
    game.ships[indexOfObject].x = user.ship.x;
    game.ships[indexOfObject].y = user.ship.y;
    game.ships[indexOfObject].dx = user.ship.dx;
    game.ships[indexOfObject].dy = user.ship.dy;
    game.ships[indexOfObject].targetX = user.ship.targetX;
    game.ships[indexOfObject].targetY = user.ship.targetY;
    game.ships[indexOfObject].dAngle = user.ship.dAngle;
    game.ships[indexOfObject].angle = user.ship.angle;
});

$(document).ready(function() {
    connection.start().then(function () {
        onLoaded();
    }).catch(function (err) {
        return console.error(err.toString());
    });
    document.getElementById("background").addEventListener("mousedown", function(e) { onClick(e.pageX, e.pageY); });
});

function onLoaded() {
    let userId = localStorage.getItem("userId");

    if (userId == null) {
        $("#btnUserNameAdded").click(function () {
            let userName = document.getElementById("userName").value;
            $.ajax({
                type: "POST",
                url: serverPath + "/user",
                data: JSON.stringify({ name: userName }),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (response) {
                    localStorage.setItem("userId", response.id);
                    localStorage.setItem("userName", response.name);
                    loadLevel(response.id);
                    connection.invoke("userConnectedAsync", response.id);
                }
            });
        });
        $('#myModal').modal("show");
    }
    else {
        loadLevel(userId);
        connection.invoke("userConnectedAsync", userId);
    }
}

function onClick(x, y) {
    let userId = localStorage.getItem("userId");
    connection.invoke("userClickAsync", userId, x, y);
}

function onGameObjectClick(id, x, y) {
    let userId = localStorage.getItem("userId");
    connection.invoke("gameObjectClickAsync", userId, id)
        .then((res) => updateScore(res));
}

function loadLevel(userId) {
    $.get(serverPath + "/level?userId=" + userId)
        .done(function (response) {
            let background = $("#background");
            background.width(response.width);
            background.height(response.height);
            background.css("background-image", "url("+response.backgroundLink+")");
            
            initialize();
            loadUser(userId);
            $("#level").text(response.title);

            for (const i in response.gameObjects) {
                const obj = response.gameObjects[i];

                if(obj.id == localStorage.getItem("userId"))
                    game.playerShip = obj;

                switch(obj.gameObjectType) {
                    case 1: game.stations.push(obj); break;
                    case 2: game.portals.push(obj); break;
                    case 3: game.ships.push(obj); break;
                    case 4: game.asteroids.push(obj); break;
                }
                game.gameObjects.push(obj);
                addGameObject(obj);
                if(obj.gameObjectType == 4)
                    document.getElementById(obj.id).addEventListener("mousedown", function(e) { e.preventDefault(); onGameObjectClick(obj.id, e.pageX, e.pageY); });
            }

            game.tick = setInterval(onTick, 30);
        });
}

function loadUser(userId) {
    $.get(serverPath + "/user?userId=" + userId)
        .done(function (response) {
            updateScore(response.score);
        });
}

function addGameObject(obj) {
    let background = $("#background");
    let divObj = $("<div id='" + obj.id + "'></div>");
    divObj.width(obj.width);
    divObj.height(obj.height);
    divObj.css("background-image", "url("+obj.imageLink+")");
    divObj.css({'position': 'absolute', 'left': 0 + "px", 'top': 0 + "px", 'display': 'none'});
    background.append(divObj);
    if(obj.title != null && obj.title != '') {
        let divTitle = $("<div id='title" + obj.id + "' class='title'></div>");
        divTitle[0].innerText = obj.title;
        divTitle.css({'position': 'absolute', 'left': 0 + "px", 'top': 0 + "px", 'display': 'none'});
        background.append(divTitle);
    }
}

function updateScore(score) {
    var scoreDiv = document.getElementById("score");
    scoreDiv.innerText = "Очки: " + score;
}

function removeGameObject(obj) {
    $('#'+obj.id).remove();
    $('#title'+obj.id).remove();
}

function initialize() {
    if(game != null && game.tick != null)
        clearInterval(game.tick);
    game.gameObjects = [];
    game.stations = [];
    game.portals = [];
    game.ships = [];
    game.asteroids = [];
    game.playerShip = {};

    $("#background").empty();

    let title = $("<div id='level'></div>");
    let score = $("<div id='score'></div>");

    $("#background").append(title);
    $("#background").append(score);
}

function onTick() {
    move(game.gameObjects);
}

function move(gameObjects) {
    for (const i in gameObjects) {
        const gameObject = gameObjects[i];
        gameObject.angle += gameObject.dAngle;
        let divObj = document.getElementById(gameObject.id);
        if(divObj != null)
        {
            divObj.style.display='block';
            const x = gameObject.x - gameObject.width / 2;
            const y = gameObject.y - gameObject.height / 2;
            divObj.style.transform = "translate(" + x + "px," + y + "px) rotate("+ (90 + gameObject.angle) +"deg)";
            let divTitle = document.getElementById("title" + gameObject.id);
            if(divTitle != null)
            {
                divTitle.style.display='block';
                const textX = gameObject.x - divTitle.offsetWidth / 2
                const textY = gameObject.y + gameObject.height / 2;
                divTitle.style.transform = "translate(" + textX + "px," + textY + "px)";
            }
        }
    }
}